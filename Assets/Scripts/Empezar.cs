﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


//Esta clase solo la uso para el parpadeo del texto y para volver a empezar a jugar.
//Tambien uso un playerpref para recuperar el tiempo transcurrido en la partida y printearlo.
public class Empezar : MonoBehaviour
{
    public GameObject press;
    public bool encendido = true;
    public Text textoTiempo;

    void Start()
    {
        textoTiempo.text = "TIEMPO\n" + (int)PlayerPrefs.GetFloat("tiempo") + " SEG";
        StartCoroutine(Parpadeo());
    }

    void Update()
    {
        ReStart();
    }

    IEnumerator Parpadeo()
    {
        while (true)
        {
            if (encendido)
            {
                press.SetActive(false);
                encendido = false;
                yield return new WaitForSeconds(0.2f);
            }
            else
            {
                press.SetActive(true);
                encendido = true;
                yield return new WaitForSeconds(1f);
            }
        }
    }

    private void ReStart()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(0);
        }
    }
}
