﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GavedadPlaneta : MonoBehaviour
{
    //Fuerza con la que el planeta atrae los objetos.
    public float gravedad = -10f;


    public void Atraccion(Transform cuerpo)
    {
        //si restamos la posicion del planeta al objeto que queremos aplicarle gravedad,
        //obtenemos la direccion de la gravedad invertida.
        //Normalizamos para que el valor maximo sea un 1.
        Vector3 gravedadUp = (cuerpo.position - transform.position).normalized;
        //Transform.up.
        Vector3 cuerpoUp = cuerpo.up;

        //Aplicamos una fuerza (de gravedad) al objeto.
        //Multiplicamos la direccion que habiamos consegido antes por la fuerza que queremos de atraccion.
        cuerpo.GetComponent<Rigidbody>().AddForce(gravedadUp * gravedad);

        //Quaternion:Con FromToRotation conseguimos una rotacion (desde un punto A, hasta un punto B)
        //Finalmente lo multimplicamos por nuestro Vector3 que nos da la parte superior de nuestro objeto.
        //Rotamos nuestro objeto para que siempre este de pie, sin inportar si esta en el norte, sur, etc.
        Quaternion rotacion = Quaternion.FromToRotation(cuerpoUp, gravedadUp) * cuerpo.rotation;
        //Ya que tenemos la rotacion, se la asignamos a nuestro objeto mediante .Slerp.
        //Quaternion.Slerp hace que la rotacion sea mas suave (Desde un pinto A, a un punto B, en este tiempo)
        cuerpo.rotation = Quaternion.Slerp(cuerpo.rotation, rotacion, 50 * Time.deltaTime);
    }
}
