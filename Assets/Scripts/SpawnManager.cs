﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    //Array donde guardo las posiciones de respawn.
    public Vector3[] posiciones;
    public bool gameOver = false;
    private int tiempoRespwn = 2;//<-------{TESTING}Tiempo que tarda en respawnear un baobab.

    void Start()
    {
        StartCoroutine(BaobabSpawn());
    }

    //Corrutina en bucle hasta que la pool no tenga mas objetos para dar.
    //Spawneamos Baobabs en intervalos de X segundo
    IEnumerator BaobabSpawn()
    {
        yield return new WaitForSeconds(2);
        while (!Pool.Instance.noHayMas)
        {
            GameObject baobab = Pool.Instance.GetBaobab();
            yield return new WaitForSeconds(tiempoRespwn);
        }
    }
    //Reactiva la corrutina BaobabSpawn(). Es llamada en evento reStart cuando se devuelve algun objeto a la pool.
    public void ReactivarCorrutina()
    {
        print("Restart");
        StartCoroutine(BaobabSpawn());
    }
}
