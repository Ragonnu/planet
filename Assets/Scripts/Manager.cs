﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Pues al final no hago mucho aqui...
//Uso un PlayerPref para guardar el tiempo transcurrido de juego y
//creo la funcion que cambiara a la escena de GameOver cuando sea llamada por el evento.
public class Manager : MonoBehaviour
{
    public string tiempo;

    private void Update()
    {
        PlayerPrefs.SetFloat("tiempo", Time.time);
    }

    public void GameOver()
    {
        SceneManager.LoadScene(1);
    }
}
