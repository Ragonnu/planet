﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSol : MonoBehaviour
{
    void Update()
    {
        //RotateAround(punto sobre el que rotar, hacia que direccion, y aqui pone angulo pero yo lo uso para la velocidad de rotacion)
        this.transform.RotateAround(new Vector3(0, 0, 0), this.transform.right, 5 * Time.deltaTime);
    }
}
