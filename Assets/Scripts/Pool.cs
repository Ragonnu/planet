﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{

    public static Pool Instance;
    public List<GameObject> baobabs;
    public ScriptableBaobab[] scriptableBaobabs;
    public GameObject baobab;
    public int CuantosObjetosEnLaPool;
    public bool noHayMas;
    public bool comprobar = false;
    public GameEvent reStart;

    //Singleton
    private void Awake()
    {
        Instance = this;
    }
    
    private void Start()
    {
        RellenarLista();
    }

    //Rellenamos la lista.
    private void RellenarLista()
    {
        baobabs = new List<GameObject>();
        for (int i = 0; i < CuantosObjetosEnLaPool; i++)
        {
            //Instanciamos un baobab.
            GameObject objeto = (GameObject)Instantiate(baobab);
            //Los desactivamos.
            objeto.SetActive(false);
            //Y le asignamos un scrptableObject que le dara posicion y velocidad de crecimiento.
            objeto.gameObject.GetComponent<Baobab>().scBaobab = scriptableBaobabs[i];
            //Finalmente, lo metemos en la lista.
            baobabs.Add(objeto);
        }
    }

    //Damos a un baobab a quien nos lo pida lsito para consumir.
    public GameObject GetBaobab()
    {
        for (int i = 0; i < baobabs.Count; i++)
        {
            //Si todos lso objetos de la Pool estan activados, retornara null, lo que hara
            //que se pare la corrutina de spawneo.
            //En cuanto descactivemos un baobab de la lista, todo se volvera a poner en marcha.
            if (!baobabs[i].activeInHierarchy)
            {
                print("Entro if");
                noHayMas = false;
                GameObject baobab = baobabs[i];
                baobab.SetActive(true);
                return baobab;
            }
        }
        print("No hay mas");
        noHayMas = true;
        comprobar = true;
        return null;
    }
    //Funcion que retorna un objeto a la pool, pero hace mucho mas...
    public void ReturnBaobab(GameObject baobab)
    {
        //Antes de retornarlo a la pool, lo desactivamos,
        baobab.SetActive(false);
        //Le volvemos a su tamaño de matojo,
        baobab.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        //Le desactivamos el bool que hace que arda.
        baobab.GetComponent<Baobab>().isOnFire = false;
        //Y lo añadimos a nuestra pool.
        baobabs.Add(baobab);

        //Finalmente, si no hay mas baobabs para dar, hacemos un notify para decir que tenemos uno("Corre que me lo quitan de las manos!")
        if(noHayMas)
        {
            //Event.
            reStart.Raise();
            noHayMas = false;
        }
    }
}
