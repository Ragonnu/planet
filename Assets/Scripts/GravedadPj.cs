﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Esta clase solo la uso para llamar continuamente la funcion de atraccion del planeta.
public class GravedadPj : MonoBehaviour
{
    public GavedadPlaneta atraccionPlaneta;
    private Transform miTransform;

    void Start()
    {
        miTransform = transform;
        atraccionPlaneta = FindObjectOfType<GavedadPlaneta>();
    }

    void Update()
    {
        atraccionPlaneta.Atraccion(miTransform);
    }
}
