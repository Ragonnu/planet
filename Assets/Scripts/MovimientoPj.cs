﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPj : MonoBehaviour
{
    private float velocidad = 5f;
    private Vector3 direccion;
    public bool Baston = false;
    private Transform child;
    public GameObject camaraUno;
    public GameObject camaraDos;
    public bool isInCamOne = true;
    public bool puedoSaltar = true;

    void Start()
    {
        //Guardo referencia al Baston de fuego.
        child = transform.GetChild(0);
    }

    void Update()
    {
        Movimiento();
        AccionBaston();
        CambioCamara();
    }
    //Funcion que cambia de una camara a otra cuando haces click derecho.
    private void CambioCamara()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (isInCamOne)
            {
                camaraDos.SetActive(true);
                camaraUno.SetActive(false);
                isInCamOne = false;
            }
            else
            {
                camaraDos.SetActive(false);
                camaraUno.SetActive(true);
                isInCamOne = true;
            }
        }
    }

    //Movimiento normal mas un saltito.
    private void Movimiento()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKey(KeyCode.LeftShift) && !Baston)
        {
            velocidad = 15;
        }
        else
        {
            velocidad = 6;
        }
        if (horizontal != 0.0f || vertical != 0.0f)
        {
            direccion = transform.forward * vertical + transform.right * horizontal;
            GetComponent<Rigidbody>().MovePosition(transform.position + direccion * velocidad * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space) && puedoSaltar)
        {
            GetComponent<Rigidbody>().AddForce(transform.up * 500);
            puedoSaltar = false;
        }
    }

    //Funcion que saca el baston de fuego con el click izquierdo.
    private void AccionBaston()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Baston)
            {
                child.gameObject.SetActive(false);
                Baston = false;
            }
            else
            {
                child.gameObject.SetActive(true);
                Baston = true;
            }
        }
    }

    //Parece ser que el FixedUpdate tiene un deltaTime mas estable.
    //Aqui juego con los quaternions para rotar a mi Principito en el eje de la Y
    //usando la Q y la E
    private void FixedUpdate()
    {
        //Cojo referencias de las direcciones del Principito.
        Vector3 alFrente = this.transform.forward;
        Vector3 alDerecha = this.transform.right;
        Vector3 alIzquierda = -this.transform.right;

        //Bueno, estos dos metodos de Quaternion los explico en GravedadPlaneta.
        if (Input.GetKey(KeyCode.E))
        {
            Quaternion rotacion = Quaternion.FromToRotation(alFrente, alDerecha) * this.transform.rotation;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            Quaternion rotacion = Quaternion.FromToRotation(alFrente, alIzquierda) * this.transform.rotation;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //si colisiono con un baobab y tengo el baston activo, llamo a la funcion OnFire
        //que quemara al baobab si es dedia.
        if (collision.gameObject.tag == "Baobab" && Baston)
        {
            print("Te quemo");
            collision.gameObject.GetComponent<Baobab>().OnFire();
        }

        //Para no saltar como un mongo.
        if (collision.gameObject.tag == "Planet")
        {
            puedoSaltar = true;
        }
    }

}
