﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baobab : MonoBehaviour
{
    private RaycastHit hit;
    public GameObject sun;
    private Transform child;
    public bool isOnFire = false;
    public bool particulaOn = false;
    public GameEvent gameOver;
    public ScriptableBaobab scBaobab;
    public float crecimiento;//<--------{TESTING}Define la velocidad de crecimiento del Baobab.
    private int tamañoMax = 10;//<--------------{TESTING}Tamaño maximo al que puede llegar un Baobab.

    void Start()
    {
        //Cargamos los datos del scriptableObject.
        this.transform.position = scBaobab.sPosicion;
        this.crecimiento = scBaobab.sCrecimiento;
        //Referencia del fuego del Baobab
        child = transform.GetChild(0);
        //Refencia del sol
        sun = GameObject.Find("Sun");
    }

    void Update()
    {
        Crecer();
        DestruirPlaneta();
    }

    //Si el Baobab crece demasiado llamamos al evento gameOver que destruira el planeta.
    private void DestruirPlaneta()
    {
        if (transform.localScale.x > tamañoMax) gameOver.Raise();
    }

    //Funcion que decide cuando debe crecer un Baobab
    private void Crecer()
    {
        //Intento de RayCast(Desde donde disparo, hacia donde disparo,ni idea...pero el hit es como el collision,
        //pillamos datos del primer objeto con el que colisiona el raycast)
        if (Physics.Raycast(transform.position, sun.transform.position, out hit))
        {
            //Usamos el hit como el collision.
            //si no esta en llamaas el baobab crecera.
            if (hit.transform.tag == "Baobab" && !isOnFire)
            {
                //print(hit.transform.tag);
                transform.localScale += new Vector3(crecimiento, crecimiento, crecimiento) * Time.deltaTime;
            }
        }
        //Si esta en llamas, el baobab disminuira su tamaño.
        if (isOnFire) transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
        //Si disminuye mucho, el Baobab desaparecera.
        //Lo devolvemos a la Pool con su funcion ReturnBaobab.
        if (transform.localScale.y <= 0.01f)
        {
            child.gameObject.SetActive(false);
            Pool.Instance.ReturnBaobab(this.gameObject);
        }
    }

    //Esta funcion sera llamada por el Principito siempre que colisione con un baobab.
    //Pero el Baobab solo se pondra en llamas si esta creciendo.
    public void OnFire()
    {
        //print("OnFire");
        if (Physics.Raycast(transform.position, sun.transform.position, out hit))
        {
            //print("Dentro");
            //print(hit.transform.tag);
            if (hit.transform.tag == "Baobab")
            {
                print("OnBaobab");
                child.gameObject.SetActive(true);
                isOnFire = true;
            }
        }
    }
}
